<?php

namespace Drupal\Tests\custom_translation_deployments\Kernel;

use Drupal\Core\File\FileSystemInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\locale\SourceString;

/**
 * Tests the locale string storage, string objects and data API.
 *
 * @group custom_translation_deployments
 */
class CustomTranslationTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'locale',
    'language',
    'system',
    'custom_translation_deployments',
    'custom_translation_deployments_test',
  ];

  /**
   * The locale storage.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->storage = $this->container->get('locale.storage');
    $this->installSchema('locale', [
      'locales_location',
      'locales_source',
      'locales_target',
      'locale_file',
    ]);
    $this->installSchema('system', [
      'sequences',
    ]);
    // Create two languages: Norwegian and Swedish.
    foreach (['nb', 'sv'] as $langcode) {
      ConfigurableLanguage::createFromLangcode($langcode)->save();
    }
    $this->installConfig('locale');
  }

  /**
   * Test that our files are imported, and the strings are translated.
   */
  public function testStringStored() {
    // Set the file path for translations to be where we can put our translation
    // files. It is there by default, but just to make sure.
    $directory = sys_get_temp_dir() . '/translations';
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = $this->container->get('file_system');
    $file_system->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $this->config('locale.settings')->set('translation.path', $directory)->save();
    // Now put our files in there.
    foreach ($this->getLanguages() as $language) {
      file_put_contents($directory . '/' . sprintf('project_specific-custom.%s.po', $language), file_get_contents(__DIR__ . '/../../assets/project_specific-custom.' . $language . '.po'));
      file_put_contents($directory . '/' . sprintf('mymodule-myversion.%s.po', $language), file_get_contents(__DIR__ . '/../../assets/mymodule-myversion.' . $language . '.po'));
    }
    // Let's try to import locale things.
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $module_handler */
    $module_handler = $this->container->get('module_handler');
    $module_handler->loadInclude('locale', 'fetch.inc');
    // Use default options.
    $translationOptions = _locale_translation_default_update_options();
    // But only use local files, since we do not have to download all projects
    // enabled to check our own functionality.
    $translationOptions['use_remote'] = FALSE;
    $batch = locale_translation_batch_update_build([], $this->getLanguages(), $translationOptions);
    batch_set($batch);
    $batch =& batch_get();
    $batch['progressive'] = FALSE;
    batch_process();
    $expected_translations = [
      'nb' => 'Forklar meg whammyens vitenskapelige natur',
      'sv' => 'Förklara för mig whammys vetenskapliga karaktär',
    ];
    $source_string = $this->storage->findString([
      'source' => 'Please explain to me the scientific nature of the whammy',
    ]);
    $this->assertStringAndTranslations($source_string, $expected_translations);
    // And we should also find the string and its translation from the test
    // module implementing the hook.
    $expected_translations = [
      'nb' => 'Det var ikke engang ekte kremost, det var lett kremost!',
      'sv' => 'Det var inte ens riktig gräddost, det var lätt gräddeost!',
    ];
    $source_string = $this->storage->findString([
      'source' => "It wasn't even real cream cheese, it was light cream cheese!",
    ]);
    $this->assertStringAndTranslations($source_string, $expected_translations);
  }

  /**
   * Helper to check the strings are as expected.
   */
  protected function assertStringAndTranslations(SourceString $source_string, $expected_translations) {
    foreach ($this->getLanguages() as $language) {
      $translation = $this->storage->findTranslation([
        'lid' => $source_string->lid,
        'language' => $language,
      ]);
      $this->assertEquals($translation->getString(), $expected_translations[$language]);
    }
  }

  /**
   * Helper.
   */
  protected function getLanguages() {
    return [
      'nb',
      'sv',
    ];
  }

}
